CREATE DATABASE Sigma_project_main;

CREATE TABLE [userReg] -- ������� � ����������������� �������
(
Id_User INT PRIMARY KEY IDENTITY(1,1), -- ��������� ����, � ��������������� �� 1
  [Name]                    NVARCHAR (50) NOT NULL, -- ��� 
  [Surname]                 NVARCHAR (50) NOT NULL, -- �������
  [email]                    VARCHAR (50)  NOT NULL, -- �����
  [password]                NVARCHAR (50) NOT NULL, -- ������
)

CREATE TABLE [Event] -- ������� �������� ���. � ��������
(
	Event_ID INT PRIMARY KEY IDENTITY(1,1), -- ��������� ����, � ��������������� �� 1
	Title nvarchar(50) NOT NULL, -- ������� �������
	Date datetime NOT NULL,    -- ���� ���������� �������
	Email_user VARCHAR (50)  NOT NULL, -- �����
	About NVARCHAR (MAX) NOT NULL, -- �������� 
	Location NVARCHAR (50) NOT NULL, -- ����� ����������
	Type NVARCHAR (50) NOT NULL, -- ��� ������� (���������, ������, �����������, �������)
	Logo NVARCHAR (MAX) NOT NULL, -- �������
)