﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Sigma_new.Models
{
    public class Event
    {
        public int ID { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Превышена максимальная длинна")]
        public string Title { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        [MaxLength(500, ErrorMessage = "Превышена максимальная длинна")]
        public string About { get; set; }

        [Required]
        [MaxLength(200, ErrorMessage = "Превышена максимальная длинна")]
        public string Location { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Превышена максимальная длинна")]
        public string Type { get; set; }

        [Required]
        [MaxLength(100, ErrorMessage = "Превышена максимальная длинна")]
        public string Logo { get; set; }

        [Required]
        [MaxLength(100, ErrorMessage = "Превышена максимальная длинна")]
        public string Email_user { get; set; }

         [Required]
         public int User_ID { get; set; }
    }
}
