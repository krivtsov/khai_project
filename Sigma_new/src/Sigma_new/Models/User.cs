﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace Sigma_new.Models
{
    public class User 
    {
        public int ID { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Превышена максимальная длинна")]
        public string Name { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Превышена максимальная длинна")]
        public string Surname { get; set; }

        [MaxLength(50, ErrorMessage = "Превышена максимальная длинна")]
        public string email { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Превышена максимальная длинна")]
        public string password { get; set; }

        //[Required]
        //[MaxLength(50, ErrorMessage = "Превышена максимальная длинна")]
        //public string Password { get; set; }

        //public string Image_Link { get; set; }
        
        //[MaxLength(13, ErrorMessage = "Превышена максимальная длинна")]
        //public string Phone_number { get; set; }

        //[Required]
        //[MaxLength(50, ErrorMessage = "Превышена максимальная длинна")]
        //public bool Mailing_flag { get; set; }

        //public DateTime Date_of_last_visit_site { get; set; }

        //public DateTime Registration_date { get; set; }
    }
}
