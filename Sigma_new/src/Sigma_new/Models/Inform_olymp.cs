﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Sigma_new.Models
{
    public class Inform_olymp
    {
                public int ID { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Превышена максимальная длинна")]
        public string Name { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Превышена максимальная длинна")]
        public string Surname { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Превышена максимальная длинна")]
        public string Patronymic { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Превышена максимальная длинна")]
        public string BirthDate { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Превышена максимальная длинна")]

        public string Email { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Превышена максимальная длинна")]
        public string HighSchoolName { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Превышена максимальная длинна")]
        public string HighSchoolAdress { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Превышена максимальная длинна")]
        public string Faculty { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Превышена максимальная длинна")]
        public string Scills { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Превышена максимальная длинна")]
        public string Course { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Превышена максимальная длинна")]
        public string Type { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Превышена максимальная длинна")]
        public string Language { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Превышена максимальная длинна")]
        public string Housing { get; set; }
    }
}
