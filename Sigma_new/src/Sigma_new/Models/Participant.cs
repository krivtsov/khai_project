﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Sigma_new.Models
{
    public class Participant
    {
        public int Id_part { get; set; }

        [Required]
        public int Id_user { get; set; }

        [Required]
        public int Id_Event { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Превышена максимальная длинна")]
        public string Surname { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Превышена максимальная длинна")]
        public string Name { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Превышена максимальная длинна")]
        public string Email { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Превышена максимальная длинна")]
        public string Phonenumber { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Превышена максимальная длинна")]
        public string Interes { get; set; }

        [Required]
        public DateTime Bday { get; set; }


        [Required]
        [MaxLength(50, ErrorMessage = "Превышена максимальная длинна")]
        public string Univ { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Превышена максимальная длинна")]
        public string Spec { get; set; }





    }
}
