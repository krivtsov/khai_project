using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Data;
using System.Data.SqlClient;
using Sigma_new.Models;
using System.Collections;


// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Sigma_new.Controllers
{
    [AllowAnonymous]
    [Route("/api/participant")]
    public class ParticipantControllerController : Controller
    {




        [HttpGet("{Id_Event}")]
        public DataTable Get(int Id_Event)
        {

            Console.WriteLine("---->" + Id_Event);
            return DB.GetParticipant(Id_Event);
        }

        [HttpPost]
        public void Post([FromBody] Participant value)
        {
            Console.WriteLine(value.Id_Event + "-");
            Console.WriteLine(DB.InUserToParticip(value.Id_user,
                                                value.Id_Event,
                                                value.Surname,
                                                value.Name,
                                                value.Email,
                                                value.Phonenumber,
                                                value.Interes,
                                                value.Bday.ToString(),
                                                value.Univ,
                                                value.Spec
             ));

        }
    }
}