﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Sigma_new.Models;
using Microsoft.AspNetCore.Authorization;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Sigma_new.Controllers
{

    [AllowAnonymous]
    [Route("/api/olymp")]
    public class Olymp_infor : Controller
    {
        IHostingEnvironment _appEnv;
        // GET: api/values
   
        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        [HttpPost("{Upload}")]
        public string Post(IFormFileCollection Upload)
        {
            if (Upload == null)
            {
                Console.WriteLine("file is null");
            }
            else
            {
                Console.WriteLine("file is not null");
                Console.WriteLine(Request.Form.Files["files1"].FileName);
                Console.WriteLine(Request.Form.Files["files3"].FileName);
                using (StreamReader sr = new StreamReader(Request.Form.Files["files1"].OpenReadStream(), System.Text.Encoding.Default))
                {
                    Console.WriteLine(sr.ReadToEnd());
                    FileStream fs = new FileStream(string.Format("{0}/files/{1}/{2}", Environment.CurrentDirectory, Request.Form["files4"], Request.Form.Files["files1"].FileName), FileMode.Create);
                    try
                    {
                        Request.Form.Files["files1"].CopyTo(fs);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Exc");
                    }
                }

                using (StreamReader sr2 = new StreamReader(Request.Form.Files["files3"].OpenReadStream(), System.Text.Encoding.Default))
                {
                    Console.WriteLine(sr2.ReadToEnd());
                    FileStream fs2 = new FileStream(string.Format("{0}/files/{1}/{2}", Environment.CurrentDirectory, Request.Form["files4"], Request.Form.Files["files3"].FileName), FileMode.Create);
                    try
                    {
                        Request.Form.Files["files3"].CopyTo(fs2);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Exc");
                    }
                }
            }
            return "Спасибо за регестрацию на олимпиаду. Письмо с информацией отправлено вам на почту.";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] Inform_olymp user)
        {
            if (user == null)
            {
                Console.WriteLine("user is null");
                if (Request.Form.Count != 0)
                {
                    Console.WriteLine("post");
                    Console.WriteLine(Request.Form.Count);
                    Console.WriteLine(Request.Form.Files["files1"]);
                    Console.WriteLine(Request.Form["files4"]);
                }
            }
            else
            {
                Directory.CreateDirectory(string.Format("{0}/files/{1}/", Environment.CurrentDirectory, user.Surname));
                Console.WriteLine("post");
                Console.WriteLine(user.Name);
                Console.WriteLine();
                Console.WriteLine(user.ID);
                Console.WriteLine(user.Surname);
                Console.WriteLine(user.Patronymic);
                Console.WriteLine(user.BirthDate);
                Console.WriteLine(user.HighSchoolName);
                Console.WriteLine(user.HighSchoolAdress);
                Console.WriteLine(user.Faculty);
                Console.WriteLine(user.Scills);
                Console.WriteLine(user.Course);
                Console.WriteLine(user.Type);
                Console.WriteLine(user.Language);
                Console.WriteLine(user.Housing);
                DB.InsertOlympUser(user);
            }

        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {

        }
    }
}
