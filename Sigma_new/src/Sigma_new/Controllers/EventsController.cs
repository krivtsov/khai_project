﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Data;
using System.Data.SqlClient;
using Sigma_new.Models;
using System.Collections;


// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Sigma_new.Controllers
{
    [AllowAnonymous]
    [Route("/api/event")]
    public class EventsController : Controller
    {

        [HttpGet("{user_email}")]
        public DataTable Get(string user_email)
        {
            Console.WriteLine("Оладушек");
            return DB.GetUsersEvent(user_email);
        }


        // GET: api/values
        [HttpGet]
        public DataTable Get()
        {

            return DB.Get_Events();
        }



        // POST api/values
        [HttpPost]
        public void Post([FromBody] Event value)
        {
            Console.WriteLine("ПИДОР");
            Console.WriteLine(DB.InsertEvent(value.Title,
                                            value.About,
                                            value.Date.ToString(),
                                            value.Email_user,
                                            value.Location,
                                            value.Logo,
                                            value.Type,
                                            value.User_ID));

        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
