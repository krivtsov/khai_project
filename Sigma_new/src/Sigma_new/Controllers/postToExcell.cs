﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using Microsoft.Office.Interop.Excel;
using System.Collections;
using Sigma_new.Models;
using System.Data.SqlClient;
// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Sigma_new.Controllers
{
    using Excel = Microsoft.Office.Interop.Excel;
    [Route("api/excell")]
    public class postToExcell : Controller
    {
        private Worksheet xlSheet;
        public Application xlApp { get; private set; }
        public Range x1SheetRange { get; private set; }
        // GET: api/values

        public void toExcell(System.Data.DataTable data_list)
        {
            xlApp = new Microsoft.Office.Interop.Excel.Application();
            try
            {
                xlApp.Workbooks.Add(Type.Missing);
                xlApp.Interactive = false;
                xlApp.EnableEvents = false;
                xlSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlApp.Sheets[1];
                xlSheet.Name = "User";
                System.Data.DataTable list = data_list;
                int collInd = 0;
                int rowInd = 0;
                string data = "";
                for (int i = 0; i < list.Columns.Count; i++)
                {
                    data = list.Columns[i].ColumnName.ToString();
                    xlSheet.Cells[1, i + 1] = data;
                    x1SheetRange = xlSheet.get_Range("A1:Z1", Type.Missing);

                    x1SheetRange.WrapText = true;
                    x1SheetRange.Font.Bold = true;
                }
                for (rowInd = 0; rowInd < list.Rows.Count; rowInd++)
                {
                    for (collInd = 0; collInd < list.Columns.Count; collInd++)
                    {
                        data = list.Rows[rowInd].ItemArray[collInd].ToString();
                        xlSheet.Cells[rowInd + 2, collInd + 1] = data;
                    }
                }
                x1SheetRange = xlSheet.UsedRange;
                x1SheetRange.Columns.AutoFit();
                x1SheetRange.Rows.AutoFit();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                xlApp.Visible = true;
                xlApp.Interactive = true;
                xlApp.ScreenUpdating = true;
                xlApp.UserControl = true;
                releaseObject(x1SheetRange);
                releaseObject(xlSheet);
                releaseObject(xlApp);
            }
        }

        [HttpGet]
        public bool Get()
        {
            Console.WriteLine("go");
            System.Data.DataTable list = DB.GetOlymp();
            toExcell(list);
            Console.WriteLine(list.ToString());
            return true;
        }

        [HttpGet("{Id_Event}")]
        public bool Get(int Id_Event)
        {
            Console.WriteLine("go" + Id_Event);
            System.Data.DataTable list = DB.GetParticipant(Id_Event);
            toExcell(list);
            Console.WriteLine(list.ToString());
            return true;
        }
        void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                throw;
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}
