﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Collections;
using Microsoft.Office.Interop.Excel;
using Sigma_new.Models;
// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Sigma_new.Controllers
{
    public class DB : Controller
    {

        static string connectionstring = @"Data Source=DESKTOP-VIK0O7G\SQLEXPRESS;Initial Catalog = Sigma_project_main;Integrated Security=True;Connect Timeout=15;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        static public bool InsertUser(string Name, string Surname, string email, string password, bool activate, bool EventMaster)
        {
            bool flag = true;
            using (SqlConnection con = new SqlConnection(connectionstring)) // закрытие соединения после считки и разрушение объекта в памяти
            {
                con.Open();
                SqlTransaction tran = con.BeginTransaction();
                SqlCommand com = con.CreateCommand();
                com.Transaction = tran;
                try
                {
                    string command = string.Format("INSERT INTO userReg VALUES('{0}','{1}','{2}','{3}','{4}','{5}') ", Name, Surname, email, password, activate, EventMaster);
                    com.CommandText = command;
                    com.ExecuteNonQuery();
                    tran.Commit();
                }
                catch (Exception)
                {
                    flag = false;
                    Console.WriteLine(new Exception());
                    tran.Rollback();
                }
                finally
                {
                    if (con.State == ConnectionState.Open)
                        con.Close();
                }
            }
            return flag;
        }

        static public bool ActivateUser(string user_email)
        {
            bool flag = true;
            using (SqlConnection con = new SqlConnection(connectionstring)) // закрытие соединения после считки и разрушение объекта в памяти
            {
                con.Open();
                SqlTransaction tran = con.BeginTransaction();
                SqlCommand com = con.CreateCommand();
                com.Transaction = tran;
                try
                {
                    string command = string.Format("UPDATE userReg SET active=1 WHERE email='{0}'", user_email);
                    com.CommandText = command;
                    com.ExecuteNonQuery();
                    tran.Commit();
                }
                catch (Exception)
                {
                    flag = false;
                    Console.WriteLine(new Exception());
                    tran.Rollback();
                }
                finally
                {
                    if (con.State == ConnectionState.Open)
                        con.Close();
                }
            }
            return flag;
        }



        static public System.Data.DataTable GetUsersEvent(string usMail)
        {
            SqlConnection con = new SqlConnection(connectionstring);
            System.Data.DataTable dt = new System.Data.DataTable();
            try

            {
                Console.WriteLine("PETUKH--" + usMail);
                string query = @"select [Event].Title,[Event].Event_ID from Event join userReg on Event.Id_user=userReg.Id_User
where userReg.email='" + usMail + "';";
                SqlCommand comm = new SqlCommand(query, con);
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(comm);
                DataSet ds = new DataSet();
                da.Fill(ds);
                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                Console.WriteLine("error get list" + ex);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
            return dt;
        }


        static public System.Data.DataTable GetOlymp()
        {
            SqlConnection con = new SqlConnection(connectionstring);
            System.Data.DataTable dt = new System.Data.DataTable();
            try
            {
                Console.WriteLine("PIDRILLA");

                string query = @"select * from olympiad ";
                SqlCommand comm = new SqlCommand(query, con);

                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(comm);
                DataSet ds = new DataSet();
                da.Fill(ds);
                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                Console.WriteLine("error get list" + ex);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
            return dt;
        }

        static public System.Data.DataTable GetParticipant(int idEv)
        {
            SqlConnection con = new SqlConnection(connectionstring);
            System.Data.DataTable dt = new System.Data.DataTable();
            try
            {
                Console.WriteLine(idEv);

                string query = @"select userReg.Name,Participant.Surname,userReg.email,Participant.Phonenumber,Participant.Bday,Participant.Interests,Participant.University,Participant.Specialization from userReg join Participant on userReg.Id_User=Participant.Id_User
 where Participant.Id_Event=" + idEv + ";";
                SqlCommand comm = new SqlCommand(query, con);

                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(comm);
                DataSet ds = new DataSet();
                da.Fill(ds);
                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                Console.WriteLine("error get list" + ex);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
            return dt;
        }



        static public bool InUserToParticip(int UsID, int EvID, string Surname,
                                                string Name,
                                                string Email,
                                                string Phonenumber,
                                                string Interes,
                                                string Bday,
                                                string Univ,
                                                string Spec)
        {
            bool flag = true;
            using (SqlConnection con = new SqlConnection(connectionstring)) // закрытие соединения после считки и разрушение объекта в памяти
            {
                con.Open();
                SqlTransaction tran = con.BeginTransaction();
                SqlCommand com = con.CreateCommand();
                com.Transaction = tran;
                try
                {
                    string command = string.Format("INSERT INTO Participant VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}')", UsID, EvID, Surname, Name, Email, Phonenumber, Interes, Bday, Univ, Spec);

                    com.CommandText = command;
                    com.ExecuteNonQuery();
                    tran.Commit();
                }
                catch (Exception)
                {

                    Console.WriteLine("ХЕР");
                    flag = false;
                    Console.WriteLine(new Exception());
                    tran.Rollback();
                }
                finally
                {
                    Console.WriteLine("НЕХЕР");
                    if (con.State == ConnectionState.Open)
                        con.Close();
                }
                return flag;
            }
        }

        static public bool InsertEvent(string title, string about, string date, string email, string location, string logo, string type, int user_id)
        {
            using (SqlConnection con = new SqlConnection(connectionstring)) // закрытие соединения после считки и разрушение объекта в памяти
            {
                Console.WriteLine(user_id);
                bool flag = true;
                con.Open();
                SqlTransaction tran = con.BeginTransaction();
                SqlCommand com = con.CreateCommand();
                com.Transaction = tran;
                try
                {
                    string command = string.Format("INSERT INTO Event VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}') ", title, date, email, about, location, type, logo, user_id);
                    com.CommandText = command;
                    com.ExecuteNonQuery();
                    tran.Commit();
                }
                catch (Exception)
                {
                    flag = false;
                    Console.WriteLine(new Exception());
                    tran.Rollback();
                }
                finally
                {
                    if (con.State == ConnectionState.Open)
                        con.Close();
                }
                return flag;
            }

        }

        static public bool InsertOlympUser(Inform_olymp user)
        {
            using (SqlConnection con = new SqlConnection(connectionstring)) // закрытие соединения после считки и разрушение объекта в памяти
            {
                bool flag = true;
                con.Open();
                SqlTransaction tran = con.BeginTransaction();
                SqlCommand com = con.CreateCommand();
                com.Transaction = tran;
                try
                {
                    string command = string.Format("INSERT INTO olympiad VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}', '{9}', '{10}', '{11}', '{12}') ",
                        user.Name, user.Surname, user.Patronymic, user.Email, user.BirthDate, user.HighSchoolName,
                         user.HighSchoolAdress, user.Faculty, user.Scills, user.Course, user.Type, user.Language, user.Housing);
                    com.CommandText = command;
                    com.ExecuteNonQuery();
                    tran.Commit();
                }
                catch (Exception)
                {
                    flag = false;
                    Console.WriteLine(new Exception());
                    tran.Rollback();
                }
                finally
                {
                    if (con.State == ConnectionState.Open)
                        con.Close();
                }
                return flag;
            }
        }


        static public System.Data.DataTable Get_woomen(string email)
        {
            SqlConnection con = new SqlConnection(connectionstring);
            System.Data.DataTable dt = new System.Data.DataTable();
            try
            {
                string query = @"SELECT * FROM userReg WHERE email='" + email + "'";
                SqlCommand comm = new SqlCommand(query, con);
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(comm);
                DataSet ds = new DataSet();
                da.Fill(ds);
                dt = ds.Tables[0];
            }
            catch { }
            finally
            {
                con.Close();
                con.Dispose();
            }
            return dt;
        }

        static public System.Data.DataTable getList(string table)
        {
            SqlConnection con = new SqlConnection(connectionstring);

            System.Data.DataTable dt = new System.Data.DataTable();
            try
            {
                string query = @"SELECT * from " + table;
                SqlCommand comm = new SqlCommand(query, con);

                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(comm);
                DataSet ds = new DataSet();
                da.Fill(ds);
                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                Console.WriteLine("error get list" + ex);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
            return dt;
        }


        //Запросы для ивентов
        static public System.Data.DataTable Get_Events()
        {
            SqlConnection con = new SqlConnection(connectionstring);
            System.Data.DataTable dt = new System.Data.DataTable();
            try
            {
                string query = @"SELECT * from Event";
                SqlCommand comm = new SqlCommand(query, con);

                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(comm);
                DataSet ds = new DataSet();
                da.Fill(ds);
                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                Console.WriteLine("error get list" + ex);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
            return dt;
        }

        static public bool UpdateUser(string Id, string Name, string Surname, string email, string password)
        {
            bool flag = true;
            using (SqlConnection con = new SqlConnection(connectionstring)) // закрытие соединения после считки и разрушение объекта в памяти
            {
                con.Open();
                SqlTransaction tran = con.BeginTransaction();
                SqlCommand com = con.CreateCommand();
                com.Transaction = tran;
                try
                {
                    string command = string.Format("UPDATE userReg SET Name='{0}', Surname ='{1}', email='{2}' , password='{3}' WHERE Id_User = '{4}'", Name, Surname, email, password, Id);
                    com.CommandText = command;
                    com.ExecuteNonQuery();
                    tran.Commit();
                }
                catch (Exception)
                {
                    flag = false;
                    Console.WriteLine(new Exception());
                    tran.Rollback();
                }
                finally
                {
                    if (con.State == ConnectionState.Open)
                        con.Close();
                }
            }
            return flag;
        }
    }
}
