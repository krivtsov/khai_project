﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Data;
using System.Data.SqlClient;
using Sigma_new.Models;
using System.Collections;
using RestSharp;
using RestSharp.Authenticators;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Sigma_new.Controllers
{


    [AllowAnonymous]
    [Route("/api/registr")]
    public class RegistrController : Controller
    {
        // GET: api/values
        [HttpGet("{email}")]
        public DataTable Get(string email)
        {
            Console.WriteLine(email);
            return DB.Get_woomen(email);
        }
        // POST api/values
        [HttpPost("{email}")]
        public void Post(string email)
        {

            Console.WriteLine(email);
            Console.WriteLine(DB.ActivateUser(email));
        }

        [HttpPost]
        public void Post([FromBody] User value)
        {


            RestClient client = new RestClient();
            string str = "https://api.mailgun.net/v3";
            client.BaseUrl = new Uri(str);
            client.Authenticator =
           new HttpBasicAuthenticator("api",
                                      "key-6fd1d909234ef2a63fd894c1dd6cc325");
            RestRequest request = new RestRequest();
            request.AddParameter("domain",
                                "sandboxfcecb985dc3d4c809a1a315ce3a8ddb9.mailgun.org", ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", "Mailgun Sandbox <postmaster@sandboxfcecb985dc3d4c809a1a315ce3a8ddb9.mailgun.org>");
            request.AddParameter("to", value.email);
            request.AddParameter("subject", "Hello " + value.Name);
            request.AddParameter("html", "<p>Поздравляю, " + value.Name + " ,ты почти стал образованым и правильным человеком, для окончания сего процесса перейди по ссылке,не будь петухом ----> <a href='http://localhost:5000#activate/" + value.email + "'>Перейти по ссылке</a></p>");
            request.Method = Method.POST;
            Console.WriteLine(client.Execute(request));


            Console.WriteLine(DB.InsertUser(value.Name.ToString(),
                                            value.Surname.ToString(),
                                            value.email.ToString(),
                                            value.password.ToString(),
                                            false, false));
        }

        // PUT api/values/5
        [HttpPut]
        public void Put([FromBody] User value)
        {
            Console.WriteLine(value.ID);
            Console.WriteLine(value.Name);
            Console.WriteLine(value.Surname);
            Console.WriteLine(value.email);
            Console.WriteLine(value.password);
            Console.WriteLine(DB.UpdateUser(value.ID.ToString(),
                                                value.Name,
                                                value.Surname,
                                                value.email,
                                                value.password));
        }



        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
