﻿using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace Mocoding.Ofx.Server.Middlewares
{
    public class Html5Middleware
    {
        private readonly RequestDelegate _next;
        readonly string _defaultFilePath;
        readonly IHostingEnvironment _env;

        public Html5Middleware(RequestDelegate next, IHostingEnvironment env)
        {
            _next = next;
            _defaultFilePath = Path.Combine(env.WebRootPath, "index.html");
            _env = env;
        }

        public async Task Invoke(HttpContext context)
        {
            var needHtml = context.Request.Headers.FirstOrDefault(k => k.Key == "Accept");

            if (needHtml.Key != null && needHtml.Value[0].Contains("text/html"))
            {
                context.Response.ContentType = "text/html";

                var html = File.ReadAllBytes(_defaultFilePath);

                await context.Response.Body.WriteAsync(html, 0, html.Length);
            }
            else
                await _next.Invoke(context);

        }
    }
}
